
source('./qmra/treatment_reduction/exposure_model.r')
source('./qmra/treatment_reduction/treatment.r')
source('./qmra/treatment_reduction/ncstr.r')

require(jsonlite)
referenceData <- fromJSON('./qmra/treatment_reduction/data/reference_data.json')
trOrganisms <- fromJSON('./qmra/treatment_reduction/data/organisms.json')
defaultSlice <- 0.002
frameSize <- (1 - defaultSlice) / defaultSlice

treatmentReductionPage <- function(id, label = 'Treatment Reduction'){
  ns <- NS(id)
  div(
    shinyjs::useShinyjs(),
    sidebarPanel(
      h3('Population Parameters'),
      numericInput('population', label='Population', 1000000, min = 10, max = 10e9),
      numericInput('consumption', label='Daily Consumption (l)', 1, min = 0, max = 100),
      h3('Organism Parameters'),
      fluidRow(
        column(4, span('Cryptospyridium')), 
        column(4, numericInput('cryptoMean', '', 10)),
        column(4, numericInput('cryptoSD', '', 10))
      ),
      fluidRow(
        column(4, span('Giardia')), 
        column(4, numericInput('giardiaMean', '', 10)),
        column(4, numericInput('giardiaSD', '', 10))
      )
    ),
    mainPanel()
  )
}

treatmentReductionModule <- function(inputs, outputs, session){

  organisms <- trOrganisms

  # TODO Disinfection
  # In previous iterations, this was done on the client side.
  # t1 <- opts$treatOpts1
  # t2 <- opts$treatOpts2
  # disinfectRed1 <- opts$dis1
  # disinfectRed2 <- opts$dis2
  prOfInf <- data.frame(crypto = c(0),giardia = c(0), rota = c(0), campy = c(0), eColi = c(0))
  annRisk <- data.frame(crypto = c(1:frameSize), giardia = c(1:frameSize), rota = c(1:frameSize), campy = c(1:frameSize), eColi = c(1:frameSize))
  sliceInfo <- data.frame(crypto = c(1:frameSize), giardia = c(1:frameSize), rota = c(1:frameSize), campy = c(1:frameSize), eColi = c(1:frameSize))
  
  # for(org in organisms){
  #   name <- org$short
  #   optimParam <- c(org$optimizedParam, org$beta)
  #   totalLR <- opts$totalLR[[name]]
  #   respData <- DoseResponse(opts[[name]][["mean"]], opts[[name]][["sd"]], totalLR, org$model, optimParam)
  #   annRisk[name] <- AnnualRiskOfIllness(respData$sumProbs, org$illGivenInfect)
  #   sliceInfo[name] <-respData$normPDF
  #   prOfInf[name][1] <- sum(respData$wPrOfInf)
  # }
  # 
  # results <- list(annRiskIllness = annRisk, dpinf = prOfInf, normPDF = sliceInfo)
  # 
  # TODO DALYS
  # Add calcs for disability adjusted life years.
  
  #return(results)
}


Disinfect <- function(type, hrt, BF, temp, initConc, kValue, pH){
  if(type == "none"){
    return(0)
  } else {
    rc <- ReactorChar(hrt, BF, temp, pH, kValue, initConc)
    li <- LogInactivation(rc, temp, pH)
    ro <- RemainingOrganisms(rc, li)
    red <- data.frame(crypto=c(0),giardia=c(0),rota=c(0),campy=c(0),eColi=c(0))
    red$crypto[1] <- -log10(sum(ro[[type]][["crypto"]]) / 1E50)
    red$giardia[1] <- -log10(sum(ro[[type]][["giardia"]]) / 1E50)
    red$rota[1] <- -log10(sum(ro[[type]][["rota"]]) / 1E50)
    red$campy[1] <- -log10(sum(ro[[type]][["campy"]]) / 1E50)
    red$eColi[1] <- -log10(sum(ro[[type]][["eColi"]]) / 1E50)
    return(red)
  }
}

DoseResponse <- function(mean, sd, logRed, model, optimParam, water = 1.0){
  estLnMean <- EstimatedLnMean(mean, sd)
  estLnSD <- EstimatedLnSD(mean, sd)
  sliceProbs <- SliceProbabilities(defaultSlice, estLnMean, estLnSD)
  treatWater <- TreatedWaterPathConc(sliceProbs, logRed, water)
  sumProbs <- SumOfProbsForEachConc(sliceProbs,treatWater, model, optimParam)
  drData <- data.frame(sumProbs=sumProbs)
  drData$wPrOfInf <- WeightedDailyProbOfInf(drData$sumProbs, sliceProbs, model, optimParam)
  drData <- cbind(drData, sliceProbs)
  return(drData)
}
