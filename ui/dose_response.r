require(shiny)
require(shinyjs)
require(dplyr)
require(ggplot2)
source('./qmra/dose_response/models.r')
source('./qmra/dose_response/bootstrap.r')
source('./qmra/dose_response/plots.r')

drOrganisms <- read.csv('./qmra/dose_response/data/dr_organisms.csv')

doseResponsePage <- function(id, label = 'Dose Response'){
  
  ns <- NS(id)
  div(
  shinyjs::useShinyjs(),
  sidebarPanel(
    tabsetPanel(type = "tabs",
      tabPanel("Point Estimate", 
         numericInput(
           ns('dose'),
           label = 'Dose',
           1,
           min = 1,
           max = 1e10,
           step = 1
         ),
        selectInput(ns('organism'),'Organism',
          c('Custom', as.character(drOrganisms$name))
        ),
        selectInput(ns('model'), 'Model',
          c(Exponential = "exponential", BetaPoisson = "beta-Poisson")
        ),
  conditionalPanel(
    condition = "input.model == 'exponential'", ns = ns,
    numericInput(
      ns('k'),
      label = 'k',
      5e-6,
      min = 1e-10,
      max = 1e10,
      step = 0.01
    )
  ),
  conditionalPanel(
    condition = "input.model == 'beta-Poisson'", ns = ns,
    numericInput(
      ns('alpha'),
      label = 'alpha',
      90,
      min = 1e-10,
      max = 1e10,
      step = 0.1
    ),
    numericInput(
      ns('N50'),
      label = 'N50',
      1.25e6,
      min = 1,
      max = 1e10,
      step = 1
    )
  )),
  tabPanel("Bootstrap", div(
    fileInput(ns("dr_file"), 
      "Upload Dose Response File",
      accept = c(
        "text/csv",
        "text/comma-separated-values,text/plain",
        ".csv")
    ),
  )))
),
mainPanel(
  h2('Dose Response'),
  div(plotOutput(ns('responseCurve'))),
  h3(verbatimTextOutput(ns('responseProb'))),
  uiOutput(ns('dataInput')),
  uiOutput(ns('bootstrapOut'))
  )        
)
}

doseResponseModule <- function(input, output, session) {
  
  observeEvent(input$organism, {
    if(input$organism == 'Custom'){
      shinyjs::enable("model")
      shinyjs::enable("k")
      shinyjs::enable("alpha")
      shinyjs::enable("N50")
    } else {
      org <- drOrganisms %>% filter(name == input$organism)
      
      updateSelectInput(session, "model", selected = org$best_fit_model[1])
      shinyjs::disable("model")
      updateNumericInput(session, "k", value = org$k[1])
      shinyjs::disable("k")
      updateNumericInput(session, "alpha", value = org$alpha[1])
      shinyjs::disable("alpha")
      updateNumericInput(session, "N50", value = org$N50[1])
      shinyjs::disable("N50")
    }
  })

  drPlot <- reactive({
    if (input$model == 'exponential') {
      DRExponentialCurve(input$k, input$dose)
    } else {
      DRBetaPoissonCurve(input$alpha, input$N50, input$dose)
    }
  })
  
  drProb <- reactive({
    if (input$model == 'exponential') {
      DRExponential(input$k, input$dose)
    } else {
      DRBetaPoisson(input$alpha, input$N50, input$dose)
    }
  })
  
  output$responseCurve <- renderPlot(drPlot())
  output$responseProb <- renderPrint(drProb())
  
  observeEvent(input$dr_file, {
    drData <<- read.csv(input$dr_file$datapath, header=TRUE)
    output$dataInput <- renderUI(div(
      h2('Uploaded Experimental Data'),
      renderTable(drData),
    ))
    results <- DRBootstrap(drData)
    output$bootstrapOut <- renderUI(div(
      h2('Un-bootstrapped MLE Exponential'),
      renderTable(results$MLEexp),
      h2('Un-bootstrapped MLE beta-Poisson'),
      renderTable(results$MLEbp),
      h2('Goodness Fit'),
      renderTable(results$goodness),
      h2('Best Fit Results'),
      renderTable(results$best),
      h2('Model Curves'),
      renderPlot(DRMultiCurve('exponential', results$MLEexp, drData, results$bootExp)),
      renderPlot(DRMultiCurve('beta-Poisson', results$MLEbp, drData, results$bootBP))
    ))
  })
}