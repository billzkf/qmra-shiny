require(shiny)
source('./ui/home.r')
source('./ui/dose_response.r')
source('./ui/treatment_reduction.r')


ui <- navbarPage("QMRA",
    # App
    tabPanel("Home", homePage),
    tabPanel("Dose Response", doseResponsePage('dr-page')),
    tabPanel("Treatment Reduction", treatmentReductionPage('tr-page')),
    tabPanel("Help")
)

server <- function(input, output, session){
  callModule(doseResponseModule, 'dr-page')
  callModule(treatmentReductionModule, 'tr-page')
}

shinyApp(ui, server)