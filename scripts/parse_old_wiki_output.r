require(jsonlite)

orgsRaw <- fromJSON('~/projects/qmra_ionic/www/apimock/organisms.json')$query$results
orgNames <- names(orgsRaw)
orgCount <- as.numeric(length(orgNames))

out <- data.frame(
  stringsAsFactors=FALSE,
  name = as.character(orgCount),
  best_fit_model = as.character(orgCount),
  k = numeric(orgCount),
  alpha = numeric(orgCount),
  N50 = numeric(orgCount),
  host = character(orgCount),
  agent_strain = character(orgCount),
  route = character(orgCount),
  doses = character(orgCount),
  dose_unit = character(orgCount),
  response = character(orgCount),
  reference = character(orgCount)
)

i <- 1
for (org in orgNames) {
  name <- strsplit(org,':')[[1]][1]
  out$name[i] <- name
  
  o <- orgsRaw[[org]]$printouts
  pms <- strsplit(unlist(strsplit(o$Parameter,'[,]')),'[=]')
  
  out$N50[i] <- as.numeric(o$N50)
  if (o$`Best Fit Model` == 'exponential'){
    out$k[i] = as.numeric(pms[[1]][2])
  } else {
    if (trimws(pms[[1]][1])%in% c('&#945;', '&alpha;')){
      out$alpha[i] <- as.numeric(pms[[1]][2])
      out$N50[i] <- as.numeric(pms[[2]][2])
    } else {
      out$alpha[i] <- as.numeric(pms[[2]][2])
      out$N50[i] <- as.numeric(pms[[1]][2])
    }
  }
  out$best_fit_model[i] <- o$`Best Fit Model`
  
  out$host[i]  <- o$Host
  out$agent_strain[i]  <- ifelse(class(o$`Agent Strain`) == 'list', '', o$`Agent Strain`)
  out$route[i]  <- o$Route
  out$doses[i]  <- o$Dose
  out$dose_unit[i]  <- o$`Dose Unit`
  out$response[i]  <- o$Response
  out$reference[i]  <- o$Reference
  i <- i + 1
}

write.csv(out, '~/projects/qmra-shiny/data/dr_organisms.csv')
