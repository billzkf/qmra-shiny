DRExponential <- function(k, dose) {
  #Computes the exponential Dose Response model
  #Args:
  #  k: Best fit parameter
  #  dose: this dose
  result <- 1 - exp(-k * dose)
  return(result)
}

RDExponential <- function(k, response) {
  #Computes the dose from a provided response
  #Args:
  #  k: Best fit parameter
  #  response: the response
  dose = log(1 / (1 - response)) / k;
  return(dose)
}

DRBetaPoisson <- function(alpha, N50, dose){
  #Computes the BP Dose Response model
  #Args:
  #  alpha: parameter
  #  N50: dose for 50-50 response
  #  dose: this dose
  result <- 1 - (1 + (dose / N50) * (2 ^ (1 / alpha) - 1)) ^(-alpha)
  return(result)
}

RDBetaPoisson <- function(alpha, N50, response){
    #Computes the dose from a provided response for the Beta-Poisson model
    #Args:
    #  alpha: parameter
    #  N50: dose for 50-50 response
    #  response: this response 
    dose =  - (-log(response + 1) / log(alpha) + 1) / (2 ^ (1 / alpha) / N50)
    return(dose)
}
