require(jsonlite)
require(stats4)
require(boot)
require(car)

DRExponentialDev <- function(obspos, obsneg, logk, dose){
  #Deviance of the exponential model to the data
  #Args:
  #  obspos: positive response
  #  obspos: negative response

  eps <- 1e-15       #ensures that the function will not divide by zero
  k <- exp(logk)
  obsf <- obspos / (obspos + obsneg);
  pred <- DRExponential(k, dose);
  y1 <- sum(obspos * log(pred / (obsf + eps)));
  y2 <- sum(obsneg * log((1 - pred + eps) / (1 - obsf + eps)))
  result <- -1 * (y1 + y2)
  return(result)
}

DRBetaPoissonDev <- function(obspos, obsneg, logalpha, logN50, dose){
  #Deviance of the bp model to the data
  eps <- 1e-15;
  alpha <- exp(logalpha)
  N50 <- exp(logN50)
  obsf <- obspos / (obspos + obsneg);
  pred <- DRBetaPoisson(alpha, N50, dose);
  y1 <- sum(obspos * log(pred / (obsf + eps)));
  y2 <- sum(obsneg * log((1 - pred + eps) / (1 - obsf + eps)))
  result <- (-1 * (y1 + y2))
  return(result)
}

DRExponentialMLE <- function(DRData, toFile = FALSE){
  #MLE for Exponential
  d <- DRData
  results <- mle(DRExponentialDev, start = list(logk = -11), method = 'BFGS',
    fixed = list(obspos = d$positive_response, obsneg = d$negative_response, dose = d$dose))
  EXP_MLE <- matrix(ncol = 3, nrow = 1)
  YEget <- logLik(results)
  YE <- -2 * YEget[[1]]
  j <- coef(results)
  logk <- j["logk"]
  k <- exp(logk)
  Exp_ID50=(log(-1 / (0.5 - 1)) / k)
  EXP_MLE[, 1] = YE
  EXP_MLE[, 2] = k
  EXP_MLE[, 3] = Exp_ID50

  if (toFile == TRUE){
    colnames(EXP_MLE) <- c("Minimized Deviance","k Parameter","50% Probability Dose (Lethal or Infectious)")
	  rownames(EXP_MLE) <- ("") # FIXME - why is this here?
	  write.csv(EXP_MLE, file="Exponential_MLE_output.csv")
  }

  return(EXP_MLE)
}

DRBetaPoissonMLE <- function(DRData, toFile = FALSE){
  #MLE for Beta Poisson
  d <- DRData
  resultsbp <- mle(DRBetaPoissonDev, start = list(logalpha = -0.2, logN50 = 10), method = 'BFGS',
    fixed = list(obspos = d$positive_response, obsneg = d$negative_response, dose = d$dose))

  BP_MLE <- matrix(ncol = 4,nrow = 1)
  YBPget <- logLik(resultsbp)
  YBP <- -2 * YBPget[[1]]
  jb <- coef(resultsbp)
  bpalpha <- exp(jb["logalpha"]);
  bpN50 <- exp(jb["logN50"])
  logalpha <- jb["logalpha"]
  logN50 <- jb["logN50"]
  BP_ID50 <- abs((0.5^(-1 / bpalpha)-1 * bpN50)/(2^(1 / bpalpha)-1))

  BP_MLE[,1] <- YBP
  BP_MLE[,2] <- bpalpha
  BP_MLE[,3] <- bpN50
  BP_MLE[,4] <- BP_ID50

  if (toFile == TRUE){
    colnames(BP_MLE)<- c("Minimized Deviance","alpha","N50","LD50 or ID50");
    rownames(BP_MLE)<-(" ") # FIXME - why is this here?
    write.csv(BP_MLE, file="betPoisson_MLE_output.csv")
  }

  return(BP_MLE)
}

DRExponentialBoot <- function(DRData, iterations, toFile =  FALSE){
  bootparms <- matrix(nrow = iterations, ncol = 4)
  d <- DRData
  total <- d$positive_response + d$negative_response
  fobs <- d$positive_response / total

  for (iter in 1:iterations) {
    bootdataframe <- d
    total <- bootdataframe$positive_response + bootdataframe$negative_response
    fobs <- bootdataframe$positive_response / total
	bootpos <- rbinom(0 + fobs, total, fobs) # draw random sample
    bootdataframe$positive_response <- bootpos # replace and form bootstrap sample
	#results_boot <- DRExponentialMLE(bootdataframe)
	results_boot <- mle(DRExponentialDev, start = list(logk = -11), method = 'BFGS',
      fixed = list(obspos = bootdataframe$positive_response,
      obsneg = total - bootdataframe$positive_response,
      dose = bootdataframe$dose))

    L <- logLik(results_boot)
    L <- 2 * L[[1]]
    jb <- coef(results_boot)
    logk_est <- jb["logk"]
    k_est <- exp(jb["logk"])
    ExpID50 <- (log(-1 / (0.5 - 1)) / k_est)

    bootparms[iter,1] <- logk_est
    bootparms[iter,2] <- k_est
    bootparms[iter,3] <- L
    bootparms[iter,4] <- ExpID50
  }
  if(toFile == TRUE){
		#TODO - see FIXMEs
		# Do this only if saving to files.
		colnames(bootparms) <- c("ln(k)","k parameter","-2 ln(Likelihood)","LD50 or ID50")
		#==========histogram of exponential k parameters from the bootstrap==========|
		png(paste("k_parameter_histogram.png", sep=''))
		par(cex = 1.2, mai = c(1.0, 1.2, 0.58, 0.15), font.lab = 1)
		hist(bootparms[, 1], breaks = 20, plot = TRUE, xlab = "ln(k)", main = " ", col = "grey54")
		dev.off()

		#==========Plot the exponential model with confidence intervals=============|
		ndiv <- 700
		klist <- exp(bootparms[, 1])
		dmin <- min(bootdataframe$dose) / 100
		ldmin <- log10(dmin)
		dmax <- max(bootdataframe$dose) * 10
		ldmax <- log10(dmax)
		diff <- (ldmax - ldmin) / ndiv
		bestfit <- matrix(nrow = ndiv + 1, ncol = 1)
		plot01 <- matrix(nrow = ndiv + 1, ncol = 1)
		plot05 <- matrix(nrow = ndiv + 1, ncol = 1)
		plot95 <- matrix(nrow = ndiv + 1, ncol = 1)
		plot99 <- matrix(nrow = ndiv + 1, ncol = 1)
		plotdose <- matrix(nrow = ndiv + 1, ncol = 1)

		for (iter in 0:ndiv + 1){
			pdose <- 10^(ldmin + (iter - 1) * diff)
			plotdose[iter] <- pdose
			bestfit[iter] <- DRExponential(exp(logk), pdose) #FIXME - where do we get the logk?
			spread <- DRExponential(klist, pdose)
			CIs <- quantile(spread, probs = c(0.005, 0.025, 0.975, 0.995))
			plot01[iter] <- CIs[1]
			plot05[iter] <- CIs[2]
			plot95[iter] <- CIs[3]
			plot99[iter] <- CIs[4]
		}
		png(paste("Exponential_model_curve.png", sep = ''))
		par(cex = 1.2, cex.lab = 1.4, mai = c(1.2, 1.0, 0.15, 0.27),
			cex.axis = 1.0, mgp = c(2.5, 0.75, 0), font.lab = 1)
		plot(bootdataframe$dose, positive / (positive + negative), log = "x", xlab = "Dose",
			ylab = "Probability of Response", ylim = c(0, 1), pch = 17, xlim = c(dmin, dmax))

		lines(plotdose, bestfit, lwd = 2)
		lines(plotdose, plot01, lty = 2, lwd = 2)
		lines(plotdose, plot05, lty = 3,lwd = 2)
		lines(plotdose, plot95, lty = 3,lwd = 2)
		lines(plotdose, plot99, lty = 2,lwd = 2)
		par(cex = 1.3)
		legend(dmin, 1, legend = c("Exponential model", "95% confidence", "99% confidence"),
			lty = c(1, 3, 2), bty = "n", y.intersp = 1.4)
		dev.off()
  }
  return(bootparms)
}

DRBetaPoissonBoot <- function(DRData, iterations, toFile =  FALSE){
  bootparms_bp <- matrix(nrow = iterations, ncol = 6)
  d <- DRData
  for (iter in 1:iterations) {
		bootdataframe <- d
		total <- bootdataframe$positive_response + bootdataframe$negative_response
		fobs <- bootdataframe$positive_response / total
		bootpos <- rbinom(0 + fobs, total, fobs) # draw random sample
		bootdataframe$positive_response <- bootpos # replace and form bootstrap sample
		results_boot_bp <- mle(DRBetaPoissonDev, start = list(logalpha = -0.2, logN50 = 10),
			method = 'BFGS', fixed = list(obspos = bootdataframe$positive_response,
			obsneg = total-bootdataframe$positive_response,dose = bootdataframe$dose))
		#results_boot_bp
		LL <- logLik(results_boot_bp)
		LL <- 2 * LL[[1]]
		jbp <- coef(results_boot_bp)
		logN50_est <- jbp["logN50"]
		logalpha_est <- jbp["logalpha"]
		N50_est <- exp(jbp["logN50"])
		alpha_est <- exp(jbp["logalpha"])
		BPID50=abs((0.5^(-1/alpha_est)-1*N50_est)/(2^(1/alpha_est)-1))

		bootparms_bp[iter, 1] <- logalpha_est
		bootparms_bp[iter, 2] <- alpha_est
		bootparms_bp[iter, 3] <- logN50_est
		bootparms_bp[iter, 4] <- N50_est
		bootparms_bp[iter, 5] <- LL
		bootparms_bp[iter, 6] <- BPID50
  }
  if(toFile == TRUE){
  	colnames(bootparms_bp) <- c("ln(alpha)", "alpha", "ln(N50)", "N50", "-2 ln(Likelihood)", "LD50 or ID50")
  	##pdf(paste("alpha_N50_plot.pdf",sep=''))
  	png(paste("alpha_N50_plot.png",sep=''))
  	##par(cex=1.2,mai=c(1.2,1.2,0.5,0.15),font.lab=1)
  	par(cex=1,cex.lab=1.05,mai=c(0.8,0.8,0.4,0.2),cex.axis=1,mgp=c(2.5,0.75,0),font.lab=1)
  	plot(bootparms_bp[,1],bootparms_bp[,3],xlab=expression(ln(alpha)),ylab=expression(ln(N[50])))
  	dataEllipse(bootparms_bp[,1],bootparms_bp[,3],levels=0.9, center.pch=0,col="black",lwd=0.5, lty=1,plot.points=F, add=T)
  	dataEllipse(bootparms_bp[,1],bootparms_bp[,3],levels=0.95, center.pch=0,col="black",lwd=0.5, lty=2,plot.points=F, add=T)
  	dataEllipse(bootparms_bp[,1],bootparms_bp[,3],levels=0.99, center.pch=0,col="black",lwd=0.5, lty=4,plot.points=F, add=T)
  	points(x=logalpha, y=logN50, pch=21, cex=1.5, bg="gray50")
  	points(x=logalpha, y=logN50, pch=4, cex=3, font=2)
  	legend("top",legend=c("0.90 Confidence", "0.95 Confidence", "0.99 Confidence"),cex=0.90,horiz="TRUE", lty=c(1,2,3),
  	pt.cex=2, x.intersp=0.1, xjust=1, box.col=NULL, bg="white", bty='n', xpd=TRUE, inset=-0.07)
  	dev.off()

  	ndiv <- 700
  	alphalist <- exp(bootparms_bp[,1])
  	N50list <- exp(bootparms_bp[,3])
  	dmin <- min(dose)/100
  	ldmin <- log10(dmin)
  	dmax <- max(dose)*10
  	ldmax <- log10(dmax)
  	diff <- (ldmax - ldmin)/ndiv
  	bestfit_bp<-matrix(nrow=ndiv+1,ncol=1)
  	plot01_2<-matrix(nrow=ndiv+1,ncol=1)
  	plot05_2<-matrix(nrow=ndiv+1,ncol=1)
  	plot95_2<-matrix(nrow=ndiv+1,ncol=1)
  	plot99_2<-matrix(nrow=ndiv+1,ncol=1)
  	plotdose <- matrix(nrow=ndiv+1,ncol=1)

  	for (iter in 0:ndiv+1) {
  	   pdose <- 10^(ldmin + (iter-1)*diff)
  	   plotdose[iter] <- pdose
  	   bestfit_bp[iter] <- bp.dr(exp(logalpha),exp(logN50),pdose)
  	   spread <- bp.dr(alphalist,N50list, pdose)
  	   CI <- quantile(spread,probs=c(0.005,0.025,0.975,0.995))
  	   plot01_2[iter] <- CI[1]
  	   plot05_2[iter] <- CI[2]
  	   plot95_2[iter] <- CI[3]
  	   plot99_2[iter] <- CI[4]
	}

  	png(paste("Beta_Poisson_model_curve.png", sep=''))
  	par(cex=1.1,cex.lab=1.4,mai=c(1.2, 1.2,0.15,0.3),
  		cex.axis=1.1,mgp=c(2.5,0.75,0), font.lab=1)
  	plot(dose,positive/(positive+negative),log="x",xlab=expression(Dose),
  		 ylab="Probability of Response",ylim=c(0,1),pch=17, xlim=c(dmin,dmax))

  	lines(plotdose,bestfit_bp,lwd=2)
  	lines(plotdose,plot01_2,lty=2,lwd=2)
  	lines(plotdose,plot05_2,lty=3,lwd=2)
  	lines(plotdose,plot95_2,lty=3,lwd=2)
  	lines(plotdose,plot99_2,lty=2,lwd=2)
  	par(cex=1.3)
  	legend(dmin,1,legend=c("beta Poisson model","95% confidence","99% confidence"),
  		 lty=c(1,3,2),bty="n",y.intersp=1.4)
  	dev.off()
  }
  return(bootparms_bp)
}

DRExponentialBootCI <- function(bootData, toFile = FALSE){
	#Output the 95 and 99% CIs for the parameter (k)
	logk_CI <- quantile(bootData[, 1],probs = c(0.005, 0.025, 0.05, 0.95, 0.975, 0.995))
	k_CI <- data.frame(exp(logk_CI))

	EXPID50 <- quantile(bootData[, 4], probs = c(0.005, 0.025, 0.05, 0.95, 0.975, 0.995))
	EID50 <- data.frame(EXPID50)

	if(toFile == TRUE){
		colnames(k_CI) <- ("k Confidence Interval")
		colnames(EID50) <- ("ID50 Confidence Interval")
		write.csv(k_CI, file = "k_Confidence_Intervals.csv")
		write.csv(EID50, file = "Exponential_ID50_Confidence_Intervals.csv")
	}
	ExpBootCI <- list(expID50 = EXPID50, kCI = k_CI)
	return(ExpBootCI)
}

DRBetaPoissonBootCI <- function(bootData, toFile = FALSE){
	#Confidence intervals for the beta Poisson alpha parameter
	alpha_CI <- quantile(bootData[,1], probs=c(0.005, 0.025, 0.05, 0.95, 0.975, 0.995))
	ealpha_CI <- data.frame(exp(alpha_CI))

	#Confidence intervals for the beta Poisson N50 parameter
	N50_CI <- quantile(bootData[,3],probs = c(0.005,0.025,0.05,0.95,0.975,0.995))
	eN50_CI <- data.frame(exp(N50_CI))

	BPID50_CI <- quantile(bootData[,6], probs=c(0.005,0.025,0.05,0.95,0.975,0.995))
	dBPID50_CI <- data.frame(BPID50_CI)

	if(toFile == TRUE){
		colnames(ealpha_CI) <- ("alpha confidence Interval")
		colnames(eN50_CI) <- ("N50_Confidence_Intervals")
		colnames(dBPID50_CI)<-("LD50 or ID50")
		write.csv(ealpha_CI, file = "alpha_Confidence_Intervals.csv")
		write.csv(eN50_CI, file = "N50_Confidence_Interval.csv")
		write.csv(dBPID50_CI, file="LD_or_ID50_betaPoisson.csv")
	}

	BPBootCI <- list (CI_ealpha = ealpha_CI, CI_eN50 = eN50_CI, CI_dBPID50 = dBPID50_CI)

	return(BPBootCI)
}

DRGoodnessFit <- function(DRData, YE, YBP, toFile = FALSE){
  # Set up the matrix which will hold the goodness of fit results and then saved as a .csv file---|
  # And also set up the chi-squared critical values based on the degrees of freedom dof...--------|
  d <- DRData
  goodness_fit_results <- matrix(ncol = 5, nrow = 2)
  em <- nrow(d)
  dofBP <- em - 2
  dofExp <- em - 1
  gofstatBP = round(qchisq(0.95, dofBP), 4) #FIXME should be numeric. Somewhere these values are being turned character
  gofstatExp = round(qchisq(0.95, dofExp), 4)
  Exp_goodfit_pvalue <- 1 - pchisq(YE, dofExp)
  BP_goodfit_pvalue <- 1 - pchisq(YBP, dofBP)

  # Flow control to determine which conclusion to make, good fit or not------|
  # Then fill the matrix generated above with the necessary vales and conclusions----|
  if (YBP < gofstatBP) {
    gofBPgood <- "beta Poisson model shows a good fit to the data"
  } else {
    gofBPno <- "beta Poisson model does not show a good fit to the data"
  }

  if (YE < gofstatExp) {
	gofExpgood <- "Exponential model shows a good fit to the data"
  } else {
    gofExpno <- "Exponential model does not show a good fit to the data"
  }

  goodness_fit_results[1, 1] <- ("Exponential")
  goodness_fit_results[2, 1] <- ("Beta Poisson")
  goodness_fit_results[1, 2] <- round(YE, 4)
  goodness_fit_results[2, 2] <- round(YBP, 4)
  goodness_fit_results[1, 3] <- gofstatExp
  goodness_fit_results[2, 3] <- gofstatBP
  goodness_fit_results[1, 4] <- round(Exp_goodfit_pvalue, 4)
  goodness_fit_results[2, 4] <- round(BP_goodfit_pvalue, 4)

  if (YE < gofstatExp) {
    goodness_fit_results[1, 5] <- gofExpgood
  } else {
    goodness_fit_results[1, 5] <- gofExpno
	}
	if (YBP < gofstatBP) {
		goodness_fit_results[2, 5] <- gofBPgood
	} else {
		goodness_fit_results[2, 5] <- gofBPno
	}
	colnames(goodness_fit_results)<- c("MODEL","MINIMIZED DEVIANCE","CHI-SQUARED CRITICAL","CHI-SQRD P-value","CONCLUSION");
	rownames(goodness_fit_results)<- c(" ", " ")
	if(toFile == TRUE){
		# Make a .csv files which will be the goodness of fit matrix----|
		write.csv(goodness_fit_results, file = "Goodness_Fit_Results.csv")
	}

	return(goodness_fit_results)
}

DRBestFitModel <- function(gofData, toFile = FALSE) {
	bpBestFit <- FALSE
	expBestFit <- FALSE
	YE <- as.numeric(gofData[1, 2])
	YBP <- as.numeric(gofData[2, 2])
	gofstatExp <- as.numeric(gofData[1, 3])
	gofstatBP <- as.numeric(gofData[2, 3])
	# For determining best fitting model
	best_fitting_model <- matrix(ncol = 7, nrow = 2)
	bestmdlstat <- round(qchisq(0.95, 1), 4)
	deltaBPExp <- abs(YE - YBP)
	best_fit_pvalue <- 1 - pchisq(deltaBPExp, 1)

	if (deltaBPExp > bestmdlstat) {
		bestfitbothBP <- "beta Poisson model is the BEST fitting model"
		bpBestFit <- TRUE
	} else {
		bestfitbothExp <- "Exponential is the BEST fitting model"
		expBestFit <- TRUE
	}
	best_fitting_model[1, 1] <- ("Exponential")
	best_fitting_model[2, 1] <- ("Beta Poisson")
	best_fitting_model[1, 2] <- round(YE, 4)
	best_fitting_model[2, 2] <- round(YE, 4)
	best_fitting_model[2, 2] <- round(YBP, 4) #FIXME this is overwriting the same cell as the previous line.
	best_fitting_model[1, 3] <- round(deltaBPExp, 4)
	best_fitting_model[2, 3] <- round(deltaBPExp, 4)
	best_fitting_model[1, 4] <- round(bestmdlstat, 4)
	best_fitting_model[2, 4] <- round(bestmdlstat, 4)
	best_fitting_model[1, 5] <- round(best_fit_pvalue, 4)
	best_fitting_model[2, 5] <- round(best_fit_pvalue, 4)

	if (deltaBPExp < bestmdlstat) {
		best_fitting_model[1, 6] <- expBestFit
		best_fitting_model[2, 6] <- bpBestFit
		best_fitting_model[1, 7] <- bestfitbothExp
		best_fitting_model[2, 7] <- bestfitbothExp
	} else {
		best_fitting_model[1, 6] <- expBestFit
		best_fitting_model[2, 6] <- bpBestFit
		best_fitting_model[1, 7] <- bestfitbothBP
		best_fitting_model[2, 7] <- bestfitbothBP
	}

	colnames(best_fitting_model) <- c("MODEL", "MINIMIZED DEVIANCE", "DIFFERENCE BETWEEN DEVIANCES", "CHI-SQUARED CRITICAL", "CHI-SQRD P-value", "BEST FIT", "CONCLUSION")
	rownames(best_fitting_model) <- c(" ", " ")

	if(toFile == TRUE){
		# Make a .csv files which will be the goodness of fit matrix----|
		write.csv(best_fitting_model, file = "Best_Fitting_Model.csv")
	}

	return(best_fitting_model)
}

DRSuite <- function (DRData, iterations, toFile=FALSE){
	d <- DRData
	dose <- d$dose
	positive <- d$positive_response
	negative <- d$negative_response
	ni <- positive + negative

	xi <- log(dose)
	xbar <- sum(ni * xi) / sum(ni)
	pbar <- sum(positive) / sum(ni)
	Zca <- sum((xi - xbar) * positive) / sqrt((pbar * (1 - pbar)) * sum(ni * (xi - xbar) ^ 2))

	if(!is.nan(Zca)){
		if (Zca > 1.644){
			expMLE <- DRExponentialMLE(DRData, toFile)
			bpMLE <- DRBetaPoissonMLE(DRData, toFile)
			gof <- DRGoodnessFit(DRData, expMLE[1], bpMLE[1], toFile)
			bfm <- DRBestFitModel(gof, toFile)
			expBoot <- DRExponentialBoot(DRData, iterations, toFile)
			bpBoot <- DRBetaPoissonBoot(DRData, iterations, toFile)
			expCI <- DRExponentialBootCI(expBoot, toFile)
			bpCI <- DRBetaPoissonBootCI(bpBoot, toFile)

			results <- list(MLEexp = expMLE, MLEbp = bpMLE, goodness = gof, best = bfm, bootExp = expBoot, bootBP = bpBoot, CIbp = bpCI, CIexp = expCI, complete = TRUE)
			return(results)
		} else {
			results <- list(msg = "There is NOT a trend between dose and observed probability of response", complete = FALSE)
			return(results)
		}
	} else {
		results <- list(msg = "There is NOT a trend between dose and observed probability of response", complete = FALSE)
		return(results)
	}
}
