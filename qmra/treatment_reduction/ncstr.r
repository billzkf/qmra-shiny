# Code for modeling non-continously stirred tank reactor treatment reduction

RemainingOrganisms <- function(reactChar, logInact){
  
  offset <- 1E50
  slices <- nrow(reactChar)
  organisms <- c("crypto","giardia","rota","campy","eColi")
  treatments <- c("freeChlorine","chloromine","ozone","chlorineDioxide")

  fc <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  cm <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  oz <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  cd <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  results <- list(freeChlorine = fc, chloromine = cm, ozone = oz, chlorineDioxide =  cd)
  for(i in 2:slices){
    for (j in 1:length(treatments)){
      treat <- treatments[j]
      for (ii in 1:length(organisms)){
        org <- organisms[ii]
        results[[treat]][[org]][i] <- reactChar$PDF[i] * offset * 10 ^ -logInact[[treat]][[org]][i]
      }
    }
  }
  return(results)
}

LogInactivation <- function(reactChar, temp, pH){
  slices <- nrow(reactChar)

  #intv results
  fc <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  cm <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  oz <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  cd <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))

  #cumm results
  sumFC <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  sumCM <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  sumOZ <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))
  sumCD <- data.frame(crypto = numeric(slices), giardia = numeric(slices), rota = numeric(slices), campy = numeric(slices), eColi = numeric(slices))


  for(i in 2:slices){
    timeIntv <- reactChar$time[i] - reactChar$time[i - 1]

    #Free Cholorine
    fc$crypto[i] <- freeChlorine[["crypto"]](timeIntv, reactChar$residual[i])
    fc$giardia[i] <- freeChlorine[['giardia']](timeIntv, reactChar$residual[i], temp, pH)
    fc$rota[i] <- freeChlorine[["rota"]](timeIntv, reactChar$residual[i], temp, pH)
    fc$campy[i] <- freeChlorine[["campy"]](timeIntv, reactChar$residual[i])
    fc$eColi[i] <- freeChlorine[["eColi"]](timeIntv, reactChar$residual[i])

    sumFC$crypto[i] <- fc$crypto[i] + sumFC$crypto[i - 1]
    sumFC$giardia[i] <- fc$giardia[i] + sumFC$giardia[i - 1]
    sumFC$rota[i] <-  fc$rota[i] + sumFC$rota[i - 1]
    sumFC$campy[i] <-  fc$campy[i] + sumFC$campy[i - 1]
    sumFC$eColi[i] <- fc$eColi[i] + sumFC$eColi[i - 1]

    #Chloromine
    cm$crypto[i] <- chloromine[["crypto"]](timeIntv, reactChar$residual[i])
    cm$giardia[i] <- chloromine[["giardia"]](timeIntv, reactChar$residual[i], temp)
    cm$rota[i] <- chloromine[["rota"]](timeIntv, reactChar$residual[i], temp)
    cm$campy[i] <- chloromine[["campy"]](timeIntv, reactChar$residual[i])
    cm$eColi[i] <- chloromine[["eColi"]](timeIntv, reactChar$residual[i])

    sumCM$crypto[i] <- cm$crypto[i] + sumCM$crypto[i - 1]
    sumCM$giardia[i] <- cm$giardia[i] + sumCM$giardia[i - 1]
    sumCM$rota[i] <-  cm$rota[i] + sumCM$rota[i - 1]
    sumCM$campy[i] <-  cm$campy[i] + sumCM$campy[i - 1]
    sumCM$eColi[i] <- cm$eColi[i] + sumCM$eColi[i - 1]

    #Ozone
    oz$crypto[i] <- ozone[["crypto"]](timeIntv, reactChar$residual[i], temp)
    oz$giardia[i] <- ozone[["giardia"]](timeIntv, reactChar$residual[i], temp)
    oz$rota[i] <- ozone[["rota"]](timeIntv, reactChar$residual[i], temp)
    oz$campy[i] <- ozone[["campy"]](timeIntv, reactChar$residual[i])
    oz$eColi[i] <- ozone[["eColi"]](timeIntv, reactChar$residual[i])

    sumOZ$crypto[i] <- oz$crypto[i] + sumOZ$crypto[i - 1]
    sumOZ$giardia[i] <- oz$giardia[i] + sumOZ$giardia[i - 1]
    sumOZ$rota[i] <-  oz$rota[i] + sumOZ$rota[i - 1]
    sumOZ$campy[i] <-  oz$campy[i] + sumOZ$campy[i - 1]
    sumOZ$eColi[i] <- oz$eColi[i] + sumOZ$eColi[i - 1]

    #Chlorine Dioxide
    cd$crypto[i] <- chlorineDioxide[["crypto"]](timeIntv, reactChar$residual[i], temp)
    cd$giardia[i] <- chlorineDioxide[["giardia"]](timeIntv, reactChar$residual[i], temp)
    cd$rota[i] <- chlorineDioxide[["rota"]](timeIntv, reactChar$residual[i], temp)
    cd$campy[i] <- chlorineDioxide[["campy"]](timeIntv, reactChar$residual[i], temp)
    cd$eColi[i] <- chlorineDioxide[["eColi"]](timeIntv, reactChar$residual[i], temp)

    sumCD$crypto[i] <- cd$crypto[i] + sumCD$crypto[i - 1]
    sumCD$giardia[i] <- cd$giardia[i] + sumCD$giardia[i - 1]
    sumCD$rota[i] <-  cd$rota[i] + sumCD$rota[i - 1]
    sumCD$campy[i] <-  cd$campy[i] + sumCD$campy[i - 1]
    sumCD$eColi[i] <- cd$eColi[i] + sumCD$eColi[i - 1]
  }
  results <- list(freeChlorine = sumFC, chloromine = sumCM, ozone = sumOZ,
                  chlorineDioxide = sumCD, intFreeChlorine = fc, intChloromine = cm,
                  intOzone = oz, intChloDio = cd)
  return(results)
}

ReactorChar <- function(hrt, baffle, temp, pH, kValue, initConc, slices=1001){
  N <- BaffleNCSTR(baffle)
  reactorChar <- data.frame(xValue = numeric(slices))
  reactorChar$CDF <- 0
  reactorChar$PDF <- 0
  reactorChar$time <- 0
  reactorChar$residual <- 0
  reactorChar$IntCt <- 0
  
  for(i in 2:slices){
    prev <- reactorChar[i - 1,]
    reactorChar$xValue[i] = prev$xValue + qgamma(0.999, shape=N, rate=1) / 1000
    reactorChar$CDF[i] <- pgamma(reactorChar$xValue[i], N, 1)
    reactorChar$PDF[i] <- reactorChar$CDF[i] - prev$CDF
    reactorChar$time[i] <- reactorChar$xValue[i] / N * hrt
    reactorChar$residual[i] <- exp(-kValue * reactorChar$time[i]) * initConc
    reactorChar$IntCt[i] <- (initConc * -exp(-kValue * reactorChar$time[i]) / kValue) - (initConc * -exp(-kValue * reactorChar$time[1] ) / kValue)
  }
  return(reactorChar)
}


BaffleNCSTR <- function(baffle){
  N <- 2280.1 * baffle ^ 6 - 4179 * baffle ^ 5 + 2890.6 * baffle ^ 4 - 862.24 * baffle ^ 3 + 98.756 * baffle ^ 2 + 5.6287 * baffle
  return(N)
}
