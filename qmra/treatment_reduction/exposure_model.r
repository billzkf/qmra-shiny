#' ExponentialOther
#'
#' Computes the Exponential Dose Response model as used elsewhere
#' @param k parameter for model
#' @param dose the dose applied
#'
#' @return probability of response

ExponentialOther <- function(k, dose) {
  result <- 1 - exp(-k * dose)
  return(result)
}

#' Exponential
#'
#' Computes the Exponential Dose Response model
#' @param k parameter for model
#' @param dose the dose applied
#'
#' @return probability of response

Exponential <- function(k, dose){
  result <- 1 - (1 - k) ^ dose
  return(result)
}

#' Beta Poisson N50
#'
#' Computes the BP Dose Response model
#' @param alpha parameter for model
#' @param N50 dose for 50-50 response
#' @param dose the dose applied
#'
#' @return probability of response

BetaPoissonN50 <- function(alpha, N50, dose){
  result <- 1 - (1 + (dose / N50) * (2 ^ (1 / alpha) - 1)) ^ (-alpha)
  return(result)
}

#' Beta Binomial
#'
#' Computes the BB Dose Response model
#' @param alpha parameter for model
#' @param beta param
#' @param dose the dose applied
#'
#' @return probability of response

BetaBinom <- function(alpha, beta, dose){
  res <- 1 - exp(lgamma(beta + dose) + lgamma(alpha + beta) - lgamma(beta) - lgamma(alpha + beta + dose))
  return (res)
}


#' EstimatedLnMean
#'
#' The estimated ln of the mean of the concentration concentration / 100 liters
#' @param mean mean concentration of the pathogen / 100 liters
#' @param sd standard deviation of the concentration
#'
#' @return estimated mean for for ln(x)

EstimatedLnMean <- function(mean, sd){
  return (log(mean * mean / sqrt(mean * mean + sd * sd)))
}

#' EstimatedLnSD
#'
#' The estimated ln of the standard deviation of the concentration / 100 liters
#' @param mean mean concentration of the pathogen / 100 liters
#' @param sd standard deviation of the concentration
#'
#' @return estimated sd for for ln(x)

EstimatedLnSD <- function(mean, sd){
  return(sqrt(log((sd / mean) ^ 2 + 1)))
}

#' SliceProbabilities
#'
#' The raw water conc and lognormal cdf and pdf at a given interval for slices
#' columns B:F of the data_* worksheets in the excel version
#' @param interval the slice
#' @param estLnMean estimated ln of the mean
#' @param estLnSd estimate ln of the standard deviation

SliceProbabilities <- function(interval, estLnMean, estLnSd){
  sp <- data.frame(slice =  seq(from = interval, to = 1 - interval, by = interval ))
  sp$rawWaterConc <- qlnorm(sp$slice, estLnMean, estLnSd)
  sp$rawWaterLag <- qlnorm(sp$slice - interval, estLnMean, estLnSd)
  sp$cdf <- plnorm(sp$rawWaterConc, estLnMean, estLnSd)
  sp$cdfLag <- plnorm(sp$rawWaterLag, estLnMean, estLnSd)
  sp$PDF <- (sp$cdf - sp$cdfLag) / (sp$rawWaterConc - sp$rawWaterLag)
  sp$normPDF <- sp$PDF / max(sp$PDF)
  return(sp)
}

#' TreatedWaterPathConc
#'
#' Computes and returns the treated water pathogen concentration
#' @param estLnMean mean concentration of the pathogen / 100 liters
#' @param estLnSd standard deviation of the concentration
#' @param slice quantile for the raw water concentration
#' @param logReduction overall treatment reduction in concentration parameter
#'
#' @return pathogen concentration in treated water

TreatedWaterPathConc <- function(untreatedSlices, logReduction, water=1.0){
  results = data.frame(estMeanOrgsDaily=c(1:nrow(untreatedSlices)))
  results$pathConc <- untreatedSlices$rawWaterConc * 10 ^ (-1 * logReduction) / 100 # per liter again?
  results$estMeanOrgsDaily <- water * results$pathConc
  return(results)
}

#' ProbOfInfection
#'
#' Computes and returns the prob of infection for a given pathogen concentration
#' @param ingested the actual number of pathogens ingested
#' @param pathConc the per liter pathogen concentration
#' @param model the name of the model for dose-response: Exponential or BetaPoisson
#' @param modelParams vector of the params (k or alpha / N50) for the model
#' @param water the # of liters of water ingested
#'
#' @return probability of infection

ProbOfInfection <- function(ingested, pathConc,  model, modelParams, water = 1.0){
  if(model == "BetaBinom"){
    response <- BetaBinom(modelParams[1], modelParams[2], ingested)
  } else if (model == "Exponential") {
    response <- Exponential(modelParams[1], ingested)
  } else {
    stop("Not a recognized dose response model.")
  }
  concentration <- pathConc * water
  prob <- dpois(ingested, concentration) * response
  return(prob)
}

#' DailyProbOfInf
#'
#' @param estLnMean mean concentration of the pathogen / 100 liters
#' @param estLnSd standard deviation of the concentration
#' @param pathConc pathogen concentration per liter
#' @param model name of the dose response model
#' @param modelParams vector of optomized paramas for the model
#' @param minIngested minimum number of pathogens to have ingested
#' @param maxIngested minimum number of pathogens to have ingested
#' @param water liters of water ingested
#'
#' @return vector of probabilities for ingested min thru max

DailyProbOfInf <- function(estLnMean, estLnSd, pathConc, model, modelParams, minIngested=0, maxIngested=100, water=1.0){
  i <- minIngested
  idx <- 1
  probAtConc <- c(minIngested:maxIngested)
  for(i in minIngested:maxIngested){
    probAtConc[idx] <- ProbOfInfection(ingested=i, pathConc,  model, modelParams, water)
    idx <- idx + 1
  }
  return(probAtConc)
}

#' SumOfProbsForEachConc
#'
#' @param slices
#' @param treatedWater
#' @param model
#' @param modelParams
#' @param minIngested
#' @param maxIngested
#' @param water
#'
#' @return vector of the sum of probs at each slice concentration.

SumOfProbsForEachConc <- function(slices, treatedWater, model, modelParams, minIngested=0, maxIngested=100, water=1.0){
  sumsOfProb <- c(1:nrow(slices))
  for(i in sumsOfProb){
    dailyPOI <- DailyProbOfInf(estLnMean, estLnSd, treatedWater$estMeanOrgsDaily[i], model, modelParams, minIngested, maxIngested, water)
    sumsOfProb[i] <- sum(dailyPOI)
  }
  return(sumsOfProb)
}

#' WeightedDailyProbOfInf
#'
#' @param estLnMean mean concentration of the pathogen / 100 liters
#' @param estLnSd standard deviation of the concentration
#' @param interval the cdf step size for the concentration / 100L
#' @param model name of the dose response model
#' @param modelParams vector of optomized paramas for the model
#' @param minIngested minimum number of pathogens to have ingested
#' @param maxIngested minimum number of pathogens to have ingested
#' @param water liters of water ingested
#'
#' @return vector of probabilities at for ingested min thru max

WeightedDailyProbOfInf <- function(unweighted, sliceProbs, model, modelParams, minIngested=0, maxIngested=100, water=1.0){
  pOfConc <- sliceProbs$cdf - sliceProbs$cdfLag
  weighted <- pOfConc * unweighted
  return(weighted)
}


AnnualRiskOfIllness <- function(weightProbInf, pathRiskIllness){
  annRisk <- ifelse(weightProbInf < 10 ^ -6,weightProbInf * pathRiskIllness * 365,1 - ( 1 - weightProbInf * pathRiskIllness) ^ 365)
  return(annRisk)
}