# Probability Of Infection Per Year
probOfInfectionPerYear <- function(dailyPrOI) {
  if (dailyPrOI < 10e-7) {
    return(dailyPrOI * 365)
  }
  
  return(1 - (1 - dailyPrOI) ^ 365)
}

# Tests
testPrOI <- c(0.00001, 0.00000007, 0.87)
lapply(testPrOI, probOfInfectionPerYear)

probOfIllnessPerYear <- function(dailyPrOI, probOfIllGivenInf) {
  return dailyPrOI * probOfIllGivenInf
}

totalIllnessPerYear <- function(illnessPerYear) {
  return illnessPerYear * this.population
}

testOrganism <- list(DALYs=0.3457)

indDALY <- function(organism, pIllYr) {
  return (organism$DALYs * pIllYr);
}

popDALY <- function(org, pIllYr) {
  return this.organismsByName[org].DALYs * pIllYr * this.population;
}