# list of functions for free chlorine applied to various pathogens
freeChlorine <- list()

# Crypto – Free Chlorine
# Korich 1990
# 2-log demonstrated @ pH 7, 25°C; CT = 7200 mg/L-min
# level of inactivation calculated by model is capped at 4-log
freeChlorine[['crypto']] <- function(time, residual){
  result <- time * residual * 2 / 7200
  return(result)
}

# Giardia – Free Chlorine
# curve fit to data from EPA SWTR (1999)
# no safety factors applied to CT tables, but used 99% UCL statistical fit of experimental data
# 4-log inactivation demonstrated at pH=6-9
# level of inactivation calculated by model is capped at 8-log
freeChlorine[['giardia']] <- function(time, residual, temp, pH){
  result <- time * residual ^ 0.85 / (0.2828 * pH ^ 2.69 * 0.933 ^ (temp - 5))
  return(result)
}

# Rotavirus - Free Chlorine
# curve fit to original data from Sobsey, 1988 (Hepatitis A virus); data cited in EPA SWTR (1999)
# NOTE: For information, US-EPA CT tables included a safety factor of 3
# NOTE: CT values based on HAV although Rotavirus may be 20-25 times more easily inactivated
# 4-log inactivation demonstrated at 5ºC, pH 6-9
# level of inactivation calculated by model is capped at 8-log
freeChlorine[['rota']] <- function(time, residual, temp, pH){
  result <- time * residual * 0.692 * exp(0.0713 * temp) / (-0.0667 * pH ^ 3 + 1.59 * pH ^ 2 - 12.43 * pH + 32.34)
  return(result)
}

# Campylobacter – Free Chlorine
# Blaser,1986
# average values across all temperature & pH conditions cited
# proportional based on 3.64-log at CT=0.5 mg/L-min
# 3.6-log inactivation demonstrated at various pH and temperature values
# level of inactivation calculated by model is capped at 8-log
freeChlorine[['campy']] <- function(time, residual){
  result <- 3.64 / 0.5 * time * residual
  return(result)
}

# E.Coli – Free Chlorine
# Rice and Clark, 1999
# curve fit for 7 serotypes of EcoliO157
# demonstrated 4.5-log inactivation
# level of inactivation calculated by model is capped at 8-log
freeChlorine[['eColi']] <- function(time, residual){
  result <- 3.8962 * (time * residual) ^ 0.3124
  return(result)
}

chloromine <- list()

chloromine[['crypto']] <- function(time, residual){
  result <- 0.0000011 * residual ^ 2.53 * time ^ 1.28
  return(result)
}

chloromine[['giardia']] <- function(time, residual, temp){
  result <- time * residual *(0.0000068 * temp ^ 2 - 0.0000764 * temp + 0.001619)
  return(result)
}

chloromine[['rota']] <- function(time, residual, temp){
  result <- 0.2439 * (time * residual / 100) ^ 0.82 * exp(0.0588 * temp)
  return(result)
}

chloromine[['campy']] <- function(time, residual){
  result <- 3.77 / 15 * time * residual
  return(result)
}

chloromine[['eColi']] <- function(time, residual){
  result <- (2 / 64) * time * residual
  return(result)
}

ozone <- list()

ozone[['crypto']] <- function(time, residual, temp){
  ke <- exp(25.2373398441227) * exp(-62818.7684115104 / (8.314 * (273.15 + temp)))
  result  <- -log10(exp(-ke * time * residual))
  return(result)
}

ozone[['giardia']] <- function(time, residual, temp){
  result <- time * residual * (0.0087 * temp ^ 2 + 0.0334 * temp + 1.545)
  return(result)
}

ozone[['rota']] <- function(time, residual, temp){
  result <- time * residual * exp(0.7423 * exp(0.07 * temp))
  return(result)
}

ozone[['campy']] <- function(time, residual){
  result  <- -log10(exp(-499 * time * residual))
  return(result)
}

ozone[['eColi']] <- function(time, residual){
  result  <- -log10(exp(-499 * time * residual))
  return(result)
}


chlorineDioxide <- list()

chlorineDioxide[['crypto']] <- function(time, residual, temp){
  result <- 0.001506 * time * residual * 1.09116 ^ temp
  return(result)
}

chlorineDioxide[['giardia']] <- function(time, residual, temp){
  result <- time * residual * (0.0003949 * temp ^ -0.0041565 * temp + 0.12843)
  return(result)
}

chlorineDioxide[['rota']] <- function(time, residual, temp){
  result <- (time * residual) ^ 0.3854 * (0.03507 * temp + 0.82447)
  return(result)
}

chlorineDioxide[['campy']] <- function(time, residual, temp){
  if(temp < 20){
    result <- 2 * time * residual / 0.38
  } else {
    result <- 2 * time * residual / 0.18
  }
  return(result)
}

chlorineDioxide[['eColi']] <- function(time, residual, temp){
  if(temp < 20){
    result <- 2 * time * residual / 0.38
  } else {
    result <- 2 * time * residual / 0.18
  }
  return(result)
}